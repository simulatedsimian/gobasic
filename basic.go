package main

import (
	lx "bitbucket.org/chopstix/gobasic/lexer"
	"bufio"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"log"
	"os"
	"strings"
	"unicode"
)

const (
	lexerror = iota
	eof
	linenumber
	command
	operator
	number
	identifier
	keyword
	str
)

var keywords = []struct {
	name   string
	kwtype int
}{
	{"print", command},
	{"let", command},
	{"goto", command},
	{"run", command},
	{"if", command},
	{"then", keyword},
	{"and", operator},
	{"or", operator},
}

var operators = []string{
	">=",
	"<=",
	"<>",
	"<",
	">",
	"=",
	"+",
	"-",
	"/",
	"*",
}

//TODO: if in array ??

// HasPrefixI tests whether the string s begins with prefix (Case insenistive!!!)
func HasPrefixI(s, prefix string) bool {
	return len(s) >= len(prefix) && strings.EqualFold(s[0:len(prefix)], prefix)
}

func stateLinenumber(l *lx.Lexer) lx.StateFunc {
	l.DiscardRun(unicode.IsSpace)

	if len(l.Input()) == 0 {
		return nil
	}

	if l.AcceptRun(unicode.IsDigit) > 0 {
		l.EmitItem(linenumber)
	}
	return stateCommand
}

func stateCommand(l *lx.Lexer) lx.StateFunc {
	l.DiscardRun(unicode.IsSpace)

	if len(l.Input()) > 0 {
		l.AcceptRun(unicode.IsLetter)
		val := l.Current()
		if len(val) > 0 {
			for _, kw := range keywords {
				if strings.EqualFold(val, kw.name) {
					if kw.kwtype == command {
						l.EmitItem(command)
						return stateExpression
					}
				}
			}
			l.EmitError(lexerror, "Unknown command: "+val)
		}
	}

	return nil
}

func stateExpression(l *lx.Lexer) lx.StateFunc {
	l.DiscardRun(unicode.IsSpace)

	r := l.Peek()

	if r == lx.EOF {
		if r == lx.ERROR {
			l.EmitError(lexerror, "Input not valid utf-8")
		}
		return nil
	}

	if r == '"' {
		return stateString(l)
	}

	if unicode.IsDigit(r) {
		return stateNumber(l)
	}

	if unicode.IsLetter(r) {
		return stateIdenifier(l)
	}

	return stateOperator
}

func stateNumber(l *lx.Lexer) lx.StateFunc {
	if l.AcceptRun(unicode.IsDigit) > 0 {
		l.EmitItem(number)
		return stateExpression
	}

	return nil
}

func stateString(l *lx.Lexer) lx.StateFunc {
	return nil
}

func stateOperator(l *lx.Lexer) lx.StateFunc {
	l.DiscardRun(unicode.IsSpace)

	log.Println(l.Input())

	if len(l.Input()) > 0 {
		for _, op := range operators {
			if strings.HasPrefix(l.Input(), op) {
				l.AcceptRun(lx.Count(len(op)))
				l.EmitItem(operator)
				return stateExpression
			}
		}
		l.EmitError(lexerror, "Unknown Symbol starting: "+l.Input())
	}
	return nil
}

func stateIdenifier(l *lx.Lexer) lx.StateFunc {
	l.DiscardRun(unicode.IsSpace)

	l.Accept(unicode.IsLetter)
	l.AcceptRun(lx.Or(unicode.IsLetter, unicode.IsDigit, lx.Is('$')))

	if len(l.Current()) > 0 {
		for _, kw := range keywords {
			if strings.EqualFold(l.Current(), kw.name) {
				if kw.kwtype != command {
					l.EmitItem(kw.kwtype)
					return stateExpression
				}
			}
		}
		l.EmitItem(identifier)
	}

	return stateExpression
}

func lexLine(line string) []lx.Item {
	var l lx.Lexer

	defer func() {
		//		if r := recover(); r != nil {
		spew.Dump(l)
		//		}
	}()

	lx.DoLex(&l, line, stateLinenumber)
	return l.Items()
}

func main() {
	fmt.Println("[go:]")

	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("> ")

		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}

		fmt.Println(lexLine(line))
	}
}
