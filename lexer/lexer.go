package lexer

import (
	"fmt"
	"unicode/utf8"
)

const EOF rune = -1
const ERROR rune = -2

type Item struct {
	Itemtype int
	Value    string
}

func (i Item) String() string {
	return fmt.Sprint(i.Itemtype, " : ", i.Value)
}

type Lexer struct {
	input     string
	itemBegin int
	current   int
	width     int
	items     []Item
}

func (lex *Lexer) EmitItem(itemType int) {

	if lex.itemBegin == lex.current {
		panic("Nothing to Emit")
	}

	lex.items = append(lex.items, Item{itemType, lex.input[lex.itemBegin:lex.current]})
	lex.itemBegin = lex.current
}

func (lex *Lexer) EmitError(itemType int, msg string) {
	lex.items = append(lex.items, Item{itemType, msg})
}

func (lex *Lexer) DiscardItem() {
	lex.itemBegin = lex.current
}

func (lex *Lexer) Items() []Item {
	return lex.items
}

func (lex *Lexer) Input() string {
	return lex.input[lex.current:]
}

func (lex *Lexer) Current() string {
	return lex.input[lex.itemBegin:lex.current]
}

func (lex *Lexer) SetString(str string) {
	lex.input = str
	lex.itemBegin = 0
	lex.current = 0
	lex.items = []Item{}
}

func (lex *Lexer) AppendString(str string) {
	lex.input += str
}

func (lex *Lexer) Next() rune {
	r, w := utf8.DecodeRuneInString(lex.input[lex.current:])
	if r == utf8.RuneError {
		lex.width = 0
		if w == 0 {
			return EOF
		} else {
			return ERROR
		}
	}

	lex.width = w
	lex.current += w
	return r
}

func (lex *Lexer) Backup() {
	if lex.width == 0 {
		panic("nothing to Backup")
	}

	lex.current -= lex.width
	lex.width = 0
}

func (lex *Lexer) Peek() rune {
	r := lex.Next()
	if r != EOF && r != ERROR {
		lex.Backup()
	}
	return r
}

func (lex *Lexer) Accept(p Predictate) int {
	r := lex.Next()
	if p(r) {
		return 1
	}
	if r != EOF && r != ERROR {
		lex.Backup()
	}
	return 0
}

func (lex *Lexer) AcceptRun(p Predictate) int {
	var count int
	r := lex.Next()
	for ; p(r); r = lex.Next() {
		count++
	}
	if r != EOF && r != ERROR {
		lex.Backup()
	}
	return count
}

func (lex *Lexer) Verify(p Predictate) int {
	count := lex.Accept(p)
	if count > 0 {
		lex.Backup()
	}
	return count
}

func (lex *Lexer) Discard(p Predictate) int {
	count := lex.Accept(p)
	lex.itemBegin += count
	return count
}

func (lex *Lexer) DiscardRun(p Predictate) int {
	count := lex.AcceptRun(p)
	lex.itemBegin += count
	return count
}

type StateFunc func(*Lexer) StateFunc

func DoLex(l *Lexer, input string, initialState StateFunc) {
	l.SetString(input)

	for state := initialState; state != nil; {
		state = state(l)
	}
}
