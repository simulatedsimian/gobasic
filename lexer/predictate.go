package lexer

import (
	"strings"
)

type Predictate func(rune) bool

func Not(p Predictate) Predictate {
	return func(r rune) bool {
		return !p(r)
	}
}

func Or(ps ...Predictate) Predictate {
	return func(r rune) bool {
		b := false
		for _, p := range ps {
			b = b || p(r)
		}
		return b
	}
}

func And(ps ...Predictate) Predictate {
	return func(r rune) bool {
		b := true
		for _, p := range ps {
			b = b && p(r)
		}
		return b
	}
}

func AlwaysTrue(r rune) bool {
	return true
}

func AlwaysFalse(r rune) bool {
	return false
}

func Is(val rune) Predictate {
	return func(r rune) bool {
		return val == r
	}
}

func AnyOf(s string) Predictate {
	return func(r rune) bool {
		return strings.IndexRune(s, r) >= 0
	}
}

func InRange(from, to rune) Predictate {
	return func(r rune) bool {
		return r >= from && r <= to
	}
}

func Count(c int) Predictate {
	return func(r rune) bool {
		c--
		return c >= 0
	}
}
