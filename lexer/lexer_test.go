package lexer

import (
	"testing"
	"unicode"
)

func TestPredictates(t *testing.T) {
	AssertEqual(t, AlwaysTrue('x'), true)
	AssertEqual(t, AlwaysFalse('x'), false)
	AssertEqual(t, Not(AlwaysTrue)('x'), false)
	AssertEqual(t, Not(AlwaysFalse)('x'), true)

	AssertEqual(t, And(AlwaysTrue, AlwaysTrue)('x'), true)
	AssertEqual(t, And(AlwaysFalse, AlwaysTrue)('x'), false)
	AssertEqual(t, And(AlwaysTrue, AlwaysFalse)('x'), false)
	AssertEqual(t, And(AlwaysFalse, AlwaysFalse)('x'), false)

	AssertEqual(t, Or(AlwaysTrue, AlwaysTrue)('x'), true)
	AssertEqual(t, Or(AlwaysFalse, AlwaysTrue)('x'), true)
	AssertEqual(t, Or(AlwaysTrue, AlwaysFalse)('x'), true)
	AssertEqual(t, Or(AlwaysFalse, AlwaysFalse)('x'), false)

	AssertEqual(t, Is('2')('2'), true)
	AssertEqual(t, Is('2')('3'), false)

	AssertEqual(t, AnyOf("123")('2'), true)
	AssertEqual(t, AnyOf("123")('x'), false)

	AssertEqual(t, InRange('3', '5')('2'), false)
	AssertEqual(t, InRange('3', '5')('3'), true)
	AssertEqual(t, InRange('3', '5')('4'), true)
	AssertEqual(t, InRange('3', '5')('5'), true)
	AssertEqual(t, InRange('3', '5')('6'), false)
}

func TestLexer(t *testing.T) {
	var l Lexer

	l.SetString("123")

	MustPanic(t, func(t *testing.T) {
		l.Backup()
	})

	AssertEqual(t, l.Peek(), '1')
	AssertEqual(t, l.Peek(), '1')

	AssertEqual(t, l.Next(), '1')
	AssertEqual(t, l.Next(), '2')
	AssertEqual(t, l.Next(), '3')
	l.Backup()
	AssertEqual(t, l.Next(), '3')
	AssertEqual(t, l.Next(), EOF)

	l.SetString("\xbd\xb2\x3d\xbc\x20\xe2\x8c\x98")
	AssertEqual(t, l.Next(), ERROR)

	l.SetString("123")
	AssertEqual(t, l.Accept(AnyOf("ABC")), 0)
	AssertEqual(t, l.Accept(AnyOf("123")), 1)

	l.SetString("123")
	AssertEqual(t, l.AcceptRun(AnyOf("123")), 3)

	l.SetString("123")
	AssertEqual(t, l.AcceptRun(InRange('0', '9')), 3)

	l.SetString("123 Print")
	AssertEqual(t, l.AcceptRun(Or(unicode.IsDigit, unicode.IsSpace)), 4)

	l.SetString("123456")
	AssertEqual(t, l.AcceptRun(Count(3)), 3)
	AssertEqual(t, l.Input(), "456")
}

func TestLexer2(t *testing.T) {
	var l Lexer

	l.SetString(" 100   Print a")

	AssertEqual(t, l.Verify(unicode.IsSpace), 1)
	AssertEqual(t, l.Discard(unicode.IsSpace), 1)
	AssertEqual(t, l.Input(), "100   Print a")

	l.AcceptRun(unicode.IsDigit)
	AssertEqual(t, l.Input(), "   Print a")
	l.EmitItem(1)

	l.DiscardRun(unicode.IsSpace)
	AssertEqual(t, l.Input(), "Print a")

	l.AcceptRun(unicode.IsLetter)
	AssertEqual(t, l.Input(), " a")
	l.EmitItem(2)

	AssertEqual(t, len(l.Items()), 2)
	AssertEqual(t, l.Items()[0].Value, "100")
	AssertEqual(t, l.Items()[0].Itemtype, 1)
	AssertEqual(t, l.Items()[1].Value, "Print")
	AssertEqual(t, l.Items()[1].Itemtype, 2)
}
