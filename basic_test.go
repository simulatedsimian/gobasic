package main

import (
	lx "bitbucket.org/chopstix/gobasic/lexer"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"reflect"
	"testing"
)

func compareState(a, b lx.StateFunc) bool {
	return reflect.ValueOf(a).Pointer() == reflect.ValueOf(b).Pointer()
}

func verifyItems(t *testing.T, got, exp []lx.Item) error {
	if len(got) != len(exp) {
		return fmt.Errorf("Item count missmatch. \nexpected: %s\ngot: %s",
			spew.Sdump(exp), spew.Sdump(got))
	}

	return nil
}

type lexTest struct {
	name  string
	input string
	items []lx.Item
}

var lexTests = []lexTest{
	{"Single Line Number", "100", []lx.Item{{linenumber, "100"}}},
	{"100 Print", "100 Print", []lx.Item{{linenumber, "100"}}},
}

func runTests(t *testing.T, tests []lexTest) {
	for n := 0; n < len(tests); n++ {
		var l lx.Lexer
		lx.DoLex(&l, tests[n].input, stateLinenumber)

		err := verifyItems(t, l.Items(), tests[n].items)
		if err != nil {
			t.Error("Test: ", tests[n].name, " (#", n, ") Failed: ", err)
		}
	}
}

func TestStateLinenumber(t *testing.T) {

	runTests(t, lexTests)

	/*	var l lx.Lexer


		l.SetString("100")
		lx.AssertEqual(t, compareState(stateLinenumber(&l), stateCommand), true)
		verifyItems(t, l.Items(), []lx.Item{
			{linenumber, "100"},
		})

		l.SetString("print")
		lx.AssertEqual(t, compareState(stateLinenumber(&l), stateCommand), true)
		verifyItems(t, l.Items(), []lx.Item{})

		l.SetString("")
		lx.AssertEqual(t, compareState(stateLinenumber(&l), lx.StateFunc(nil)), true)
		verifyItems(t, l.Items(), []lx.Item{})

		l.SetString("     ")
		lx.AssertEqual(t, compareState(stateLinenumber(&l), lx.StateFunc(nil)), true)
		verifyItems(t, l.Items(), []lx.Item{})

		//lexVerify(t, "100 print n + n", []lx.Item{})

	*/
}
